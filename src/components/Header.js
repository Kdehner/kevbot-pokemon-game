import React from "react";
import { Row, Col } from "react-bootstrap";

const Header = () => {
  return (
    <Row className="justify-content-center mb-5">
      <Col xs={12}>
        <h1 className="text-center">Pokemon App</h1>
      </Col>
      {/* <Button onClick={props.loadSampleData}>Load Sample Data</Button> */}
    </Row>
  );
};

export default Header;
