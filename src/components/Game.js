import React, { useContext, useEffect } from "react";
import { Row, Col } from "react-bootstrap";

import firebase from "../firebase";
import { PokemonContext } from "./PokemonContext";
// import WelcomeTrainer from "./TrainerPrompts/WelcomeTrainer";
import StarterPicker from "./StarterPokemon/StarterPicker";

const Game = () => {
  const { addPokemons } = useContext(PokemonContext);

  useEffect(() => {
    const unsubscribe = firebase
      .firestore()
      .collection("pokemons")
      .orderBy("id")
      .onSnapshot((snapshot) => {
        const newPokemons = snapshot.docs.map((doc) => ({
          id: doc.id,
          ...doc.data(),
        }));

        addPokemons(newPokemons);
      });

    return () => unsubscribe();
  }, []);

  return (
    <Row>
      <Col>
        {/* <WelcomeTrainer /> */}
        <StarterPicker />
      </Col>
    </Row>
  );
};

export default Game;
