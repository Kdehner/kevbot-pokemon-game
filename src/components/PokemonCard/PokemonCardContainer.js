import React from "react";
import { Row } from "react-bootstrap";

import PokemonCard from "./PokemonCard";
import { capitalize } from "../../helpers";

const PokemonCardContainer = (props) => {
  return (
    <Row className="justify-content-around">
      {props.pokemons.map((pokemon) => {
        return (
          <PokemonCard
            key={pokemon.id}
            id={pokemon.id}
            name={capitalize(pokemon.name)}
          />
        );
      })}
    </Row>
  );
};

export default PokemonCardContainer;
