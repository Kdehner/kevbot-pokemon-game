import React from "react";
import { Card, Button, Row } from "react-bootstrap";

const PokemonCard = (props) => {
  return (
    <Card className="m-1" style={{ width: "18rem" }}>
      <Card.Img
        className="m-1"
        variant="top"
        src={`https://pokeres.bastionbot.org/images/pokemon/${props.id}.png`}
      />
      <Card.Body>
        <Card.Title className="text-center">{props.name}</Card.Title>
      </Card.Body>
      <Card.Footer>
        <Row className="justify-content-center">
          <Button>Pick</Button>
        </Row>
      </Card.Footer>
    </Card>
  );
};

export default PokemonCard;
