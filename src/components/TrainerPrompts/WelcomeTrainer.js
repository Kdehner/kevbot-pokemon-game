import React from "react";
import { Col, Row, InputGroup, FormControl, Button } from "react-bootstrap";

const WelcomeTrainer = () => {
  return (
    <Row className="justify-content-center">
      <Col xs={12} className="mb-3">
        <h3 className="text-center">Welcome Trainer! What is your name?</h3>
      </Col>
      <Col xs={6}>
        <InputGroup className="mb-3">
          <FormControl
            placeholder="Trainer Name"
            aria-label="Trainer Name"
            aria-describedby="basic-addon2"
          />
          <InputGroup.Append>
            <Button variant="outline-secondary">Submit</Button>
          </InputGroup.Append>
        </InputGroup>
      </Col>
    </Row>
  );
};

export default WelcomeTrainer;
