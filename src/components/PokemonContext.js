import React, { createContext, useReducer } from "react";

import { ADD_POKEMONS, ADD_STARTERS, pokemonReducer } from "./PokemonReducer";

export const PokemonContext = createContext();

export const PokemonProvider = (props) => {
  const defaultState = {
    pokemons: [],
    starters: [],
  };

  const [state, dispatch] = useReducer(pokemonReducer, defaultState);

  const addPokemons = (pokemons) => {
    dispatch({ type: ADD_POKEMONS, pokemons });
  };

  const addStarters = (starters) => {
    dispatch({ type: ADD_STARTERS, starters });
  };

  const { pokemons, starters } = state;

  const providerValue = {
    pokemons,
    starters,
    addPokemons,
    addStarters,
  };

  return (
    <PokemonContext.Provider value={providerValue}>
      {props.children}
    </PokemonContext.Provider>
  );
};
