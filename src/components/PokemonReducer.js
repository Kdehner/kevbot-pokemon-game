export const ADD_POKEMONS = "ADD_POKEMONS";
export const ADD_STARTERS = "ADD_STARTERS";

const addPokemons = (pokemons, state) => ({
  ...state,
  pokemons,
});

const addStarters = (starters, state) => ({
  ...state,
  starters,
});

export const pokemonReducer = (state, action) => {
  switch (action.type) {
    case ADD_POKEMONS:
      return addPokemons(action.pokemons, state);
    case ADD_STARTERS:
      return addStarters(action.starters, state);
    default:
      return state;
  }
};
