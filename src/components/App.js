import React from "react";
import { Container } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

import Header from "./Header";
import Game from "./Game";
import { PokemonProvider } from "./PokemonContext";

const App = () => {
  return (
    <PokemonProvider>
      <Container>
        <Header />
        <Game />
      </Container>
    </PokemonProvider>
  );
};

export default App;
