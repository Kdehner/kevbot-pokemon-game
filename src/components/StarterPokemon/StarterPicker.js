import React, { useContext, useEffect } from "react";
import { Row, Col, Spinner } from "react-bootstrap";

import { PokemonContext } from "../PokemonContext";
import PokemonCard from "../PokemonCard/PokemonCard";
import { capitalize } from "../../helpers";

const StarterPicker = () => {
  const { pokemons, starters, addStarters } = useContext(PokemonContext);

  useEffect(() => {
    const startersList = pokemons.filter(
      (pokemon) => pokemon.id === 1 || pokemon.id === 4 || pokemon.id === 7
    );

    addStarters(startersList);
  }, [pokemons]);

  if (starters.length <= 0) {
    return (
      <Row className="justify-content-center align-items-center">
        <Spinner animation="border" variant="secondary" role="status">
          <span className="sr-only">Loading...</span>
        </Spinner>
      </Row>
    );
  }

  return (
    <Row className="justify-content-around">
      <Col sx={12}>
        <h1 className="text-center">Pick a Starter</h1>
      </Col>
      <Col xs={12}>
        <Row className="justify-content-around">
          {starters.map((pokemon) => {
            return (
              <PokemonCard
                key={pokemon.id}
                id={pokemon.id}
                name={capitalize(pokemon.name)}
              />
            );
          })}
        </Row>
      </Col>
    </Row>
  );
};

export default StarterPicker;
