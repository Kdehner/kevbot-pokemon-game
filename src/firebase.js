import firebase from "firebase/app";
import "firebase/firestore";

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: process.env.FIREBASE_API,
  authDomain: "kevbot-pokemon-game.firebaseapp.com",
  databaseURL: "https://kevbot-pokemon-game.firebaseio.com",
  projectId: "kevbot-pokemon-game",
  storageBucket: "kevbot-pokemon-game.appspot.com",
  messagingSenderId: "580589102760",
  appId: "1:580589102760:web:8addb1745c682f1a06849d",
  measurementId: "G-Z7PX4CWP0C",
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export default firebase;
